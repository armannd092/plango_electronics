EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1875 1775 1125 1625
U 5F1DAD8F
F0 "Master" 50
F1 "Master.sch" 50
$EndSheet
$Sheet
S 4100 1775 1025 725 
U 5F3F64B5
F0 "Slave" 50
F1 "Slave.sch" 50
$EndSheet
$EndSCHEMATC
